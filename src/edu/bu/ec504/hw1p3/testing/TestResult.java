package edu.bu.ec504.hw1p3.testing;

/**
 * A class for storing test results.
 * Currently includes:
 * (i) The amount of time needed to decoded
 * (ii) The compression ratio
 * (iii) Whether the decoding test passed (true iff passed)
 */
class TestResult {

  TestResult(long theRunTime, long theOriginalSize, long theCompressedSize, boolean theTestPass) {
    runTime = theRunTime;
    originalSize = theOriginalSize;
    compressedSize = theCompressedSize;
    testPass = theTestPass;
  }

  /**
   * How long the test ran, in milliseconds.
   */
  final long runTime;

  /**
   * The size of the original file, in bytes.
   */
  final long originalSize;

  /**
   * The size of the compressed file, in bytes.
   */
  final long compressedSize;

  /**
   * Whether the test passed.
   */
  final boolean testPass;

  /**
   * @inheritDoc
   */
  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    result.append("Statistics:" + "\n");
    result.append("   Original size:   " + originalSize + "\n");
    result.append("   Compressed size: " + compressedSize + "\n");
    result.append("   Encoding time:   " + (runTime / 1000.0) + " seconds" + "\n");
    result.append("\n");
    result.append("   Compression ratio:  " +
            String.format("%.2f", ((double) originalSize / compressedSize * 100.0)) +
            "% \n");
    result.append("   Space savings:      " +
            String.format("%.2f%%", (1.0 - (double) compressedSize / originalSize)*100.0) +
            "%\n");
    result.append("Decoding test: " + (testPass ? "passed" : "FAILED!!!"));
    return result.toString();
  }
}
