package edu.bu.ec504.hw1p3.util;

import java.io.Serializable;

/**
 * An immutable atom that represents one string, and is built of two elements:
 * a.  [prefix]: marked by an integer index into some compression dictionary.
 *     The string at that index is a <i>prefix</i> of this atom's string.
 * b.  [suffix]: a string that follows the prefix to complete the atom's string.
 * The string represented by this attom is this the concatenation of
 * [prefix] and [suffix]
 */
public final class Atom implements Serializable {

    public Atom(int myIndex, String mySuffix) {
        index = myIndex;
        suffix = mySuffix;
    }

    public int getIndex() {
        return index;
    }

    public String getSuffix() {
        return suffix;
    }

    /**
     * Pretty print the atom.
     *
     * @return A human-readable string representing this atom.
     */
    @Override
    public String toString() {
        return "Atom{" + "index=" + index + ", suffix='" + suffix + '\'' + '}';
    }

    private static final long serialVersionUID = 2L; // for serialization
    private final int index;
    private final String suffix;
}
